package Logica;

import META.AgenteJpaController;
import META.LicenciaJpaController;
import META.MotivoMultaJpaController;
import META.MultaJpaController;
import META.PersonaJpaController;
import META.VehiculoJpaController;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class OrganismoTransito implements Serializable {

    private String ciudad;
    private MotivoMultaJpaController motivoMultaJpacontroller;
    private AgenteJpaController agenteJpaController;
    private PersonaJpaController personaJpaController;
    private VehiculoJpaController vehiculoJpaController;
    private LicenciaJpaController licenciaJpaController;
    private MultaJpaController multaJpaController;

    public OrganismoTransito(String ciudad) {
        this.ciudad = ciudad;
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("QUINTERO-RUEDA-CHACONPU");
        motivoMultaJpacontroller = new MotivoMultaJpaController(emf);
        agenteJpaController = new AgenteJpaController(emf);
        personaJpaController = new PersonaJpaController(emf);
        vehiculoJpaController = new VehiculoJpaController(emf);
        licenciaJpaController = new LicenciaJpaController(emf);
        multaJpaController = new MultaJpaController(emf);
    }

    public List<MotivoMulta> getMotivoMultas() {
        return motivoMultaJpacontroller.findMotivoMultaEntities();
    }

    public void addvehiculos(Vehiculo Nvehiculo) throws Exception {
        this.vehiculoJpaController.create(Nvehiculo);
    }

    public void eliminarVehiculo(String placa) throws Exception {
        this.vehiculoJpaController.destroy(placa);
    }

    public Vehiculo BuscarPlacaV(String placa) throws Exception {
        return this.vehiculoJpaController.findVehiculo(placa);
    }

    public void addMultas(Multa Agremulta) throws Exception {
        this.multaJpaController.create(Agremulta);
    }

    public void addMotivoMulta(MotivoMulta AgregaMotivomulta) throws Exception {
        this.motivoMultaJpacontroller.create(AgregaMotivomulta);
    }

    public void eliminarMMulta(String codigo) throws Exception {
        this.motivoMultaJpacontroller.destroy(codigo);
    }

    public MotivoMulta buscarMulta(String codigo) throws Exception {
        return motivoMultaJpacontroller.findMotivoMulta(codigo);
    }

    public void addAgente(Agente Agreagente) throws Exception {
        this.agenteJpaController.create(Agreagente);
    }

    public void eliminarAgente(short numeroPlaca) throws Exception {
        this.agenteJpaController.destroy(numeroPlaca);
    }

    public Agente buscarAgente(short placa) throws Exception {
        return agenteJpaController.buscarPorPlaca(placa);

    }

    public Licencia BuscarLicenciaId(long identificacion) throws Exception {
        return licenciaJpaController.buscarPorIdentificacion(identificacion);
    }
    Licencia prueba = null;

    public void addLicencias(Licencia Agrelicencia) throws Exception {
        try {
            prueba = licenciaJpaController.buscarPorIdentificacionlista(Agrelicencia.getPersona2().getIdentificacion()).get(0);
            licenciaJpaController.create(Agrelicencia);
        } catch (ArrayIndexOutOfBoundsException e) {
            personaJpaController.create(Agrelicencia.getPersona2());
            licenciaJpaController.create(Agrelicencia);
        } catch (Exception e) {
            throw new Exception("Datos incorrectos");

        }

    }

    public List<Licencia> BuscarLicencias(long identificacion) {
        return licenciaJpaController.buscarPorIdentificacionlista(identificacion);

    }

    public String getCiudad() {
        return ciudad;
    }

    public List<Vehiculo> getVehiculosOrg() {
        return vehiculoJpaController.findVehiculoEntities();
    }

    public List<Multa> getMultasOrg() {
        return multaJpaController.findMultaEntities();
    }

    public List<Licencia> getLicenciasOrg() {
        return licenciaJpaController.findLicenciaEntities();
    }

    public List<Agente> getAgentes() {
        return agenteJpaController.findAgenteEntities();
    }

}
