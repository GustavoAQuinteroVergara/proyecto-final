package Principal;

import Logica.OrganismoTransito;
import Ventanas.VentanaUI;

public class Main {

    public static void main(String[] args) throws Exception {
        OrganismoTransito organismo = new OrganismoTransito("palmira");
        VentanaUI ventana = new VentanaUI(organismo);
        ventana.setVisible(true);
        
    }
}


