package Ventanas;

import Logica.OrganismoTransito;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class VentanaUI extends javax.swing.JFrame {

    private OrganismoTransito organismo;

    public VentanaUI(OrganismoTransito organismo) {
        this.organismo = organismo;
        ManejadorVehiculo mv = new ManejadorVehiculo();
        ManejadorAgente agente = new ManejadorAgente();
        ManejadorLicencia licencia = new ManejadorLicencia();
        ManejadorMulta manejadorMulta = new ManejadorMulta();
        ManejadorMotivoMulta manejadorMotivoMulta = new ManejadorMotivoMulta();
        initComponents();
        this.setLocationRelativeTo(null);

        Exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String msg = "Programa creado por : "
                        + "\n" + "Juan Sebastian Chacón Zuluaga" + ","
                        + "\n" + "Gustavo Adolfo Quintero Vergara" + ","
                        + "\n" + "William Felipe Rueda Bejarano.";
                JOptionPane.showMessageDialog(rootPane, msg);
            }
        });
        btnrvehiculo.addActionListener(mv);
        btnragente.addActionListener(agente);
        btnrlicencia.addActionListener(licencia);
        btnregistrarmulta.addActionListener(manejadorMulta);
        btnmotivomulta.addActionListener(manejadorMotivoMulta);

        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("Data.obj")
                    );
                    stream.writeObject(organismo);
                    stream.close();
                } catch (IOException ex) {
                    Logger.getLogger(VentanaUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        screen = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        Archivo = new javax.swing.JMenu();
        Exit = new javax.swing.JMenuItem();
        btnvehiculo = new javax.swing.JMenu();
        btnrvehiculo = new javax.swing.JMenuItem();
        btnlicencia = new javax.swing.JMenu();
        btnrlicencia = new javax.swing.JMenuItem();
        bntagente = new javax.swing.JMenu();
        btnragente = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        btnregistrarmulta = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        btnmotivomulta = new javax.swing.JMenuItem();
        Ayuda = new javax.swing.JMenu();
        about = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Archivo.setText("Archivo");

        Exit.setText("Salida");
        Archivo.add(Exit);

        jMenuBar1.add(Archivo);

        btnvehiculo.setText("Vehiculo");

        btnrvehiculo.setText("Registro Vehiculo");
        btnvehiculo.add(btnrvehiculo);

        jMenuBar1.add(btnvehiculo);

        btnlicencia.setText("Licencia");

        btnrlicencia.setText("Registrar Licencia");
        btnlicencia.add(btnrlicencia);

        jMenuBar1.add(btnlicencia);

        bntagente.setText("Agente");

        btnragente.setText("Registrar Agente");
        bntagente.add(btnragente);

        jMenuBar1.add(bntagente);

        jMenu1.setText("Multa");

        btnregistrarmulta.setText("Registrar Multa");
        jMenu1.add(btnregistrarmulta);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Motivo Multa");

        btnmotivomulta.setText("Registrar Motivo Multa");
        jMenu2.add(btnmotivomulta);

        jMenuBar1.add(jMenu2);

        Ayuda.setText("Ayuda");

        about.setText("Acerca de...");
        Ayuda.add(about);

        jMenuBar1.add(Ayuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(screen, javax.swing.GroupLayout.DEFAULT_SIZE, 858, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(screen, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 630, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Archivo;
    private javax.swing.JMenu Ayuda;
    private javax.swing.JMenuItem Exit;
    private javax.swing.JMenuItem about;
    private javax.swing.JMenu bntagente;
    private javax.swing.JMenu btnlicencia;
    private javax.swing.JMenuItem btnmotivomulta;
    private javax.swing.JMenuItem btnragente;
    private javax.swing.JMenuItem btnregistrarmulta;
    private javax.swing.JMenuItem btnrlicencia;
    private javax.swing.JMenuItem btnrvehiculo;
    private javax.swing.JMenu btnvehiculo;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JDesktopPane screen;
    // End of variables declaration//GEN-END:variables

    public class ManejadorVehiculo implements ActionListener {

        private VentanaVehicu Registrovehicu = null;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (Registrovehicu == null) {
                Registrovehicu = new VentanaVehicu(organismo);
                screen.add(Registrovehicu);
            }
            Registrovehicu.setVisible(true);
        }
    }

    public class ManejadorAgente implements ActionListener {

        private VentanaAgente Registroagente = null;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (Registroagente == null) {
                Registroagente = new VentanaAgente(organismo);
                screen.add(Registroagente);
            }
            Registroagente.setVisible(true);
        }

    }

    public class ManejadorLicencia implements ActionListener {

        private VentanaLicencia RegistroLicencia = null;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (RegistroLicencia == null) {
                RegistroLicencia = new VentanaLicencia(organismo);
                screen.add(RegistroLicencia);

            }
            RegistroLicencia.setVisible(true);
        }
    }

    public class ManejadorMulta implements ActionListener {

        private VentanaMulta RegistroMulta = null;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (RegistroMulta == null) {
                RegistroMulta = new VentanaMulta(organismo);
                screen.add(RegistroMulta);
            }
            RegistroMulta.setVisible(true);
        }

    }

    private class ManejadorMotivoMulta implements ActionListener {

        private VentanaMotivoMulta motivoMulta = null;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (motivoMulta == null) {
                motivoMulta = new VentanaMotivoMulta(organismo);
                screen.add(motivoMulta);
            }
            motivoMulta.setVisible(true);

        }

    }

}
