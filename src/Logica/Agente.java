package Logica;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
        @NamedQuery(name = "buscar_agente_por_placa",
                query = "SELECT a FROM Agente a WHERE a.numeroPlaca = :numeroplaca"))
public class Agente extends Persona {

    @Column(nullable = false, unique = true)
    private short numeroPlaca;

    public Agente() {
    }

    public Agente(short numeroPlaca, long identificacion, String nombre, String apellido) throws Exception {
        super(identificacion, nombre, apellido);
        setNumeroPlaca(numeroPlaca);
    }

    public short getNumeroPlaca() {
        return numeroPlaca;
    }

    public void setNumeroPlaca(short numeroPlaca) throws Exception {
        if (numeroPlaca < 0 || numeroPlaca > 9999999) {
            throw new Exception("EL número del agente no es valido");
        }
        this.numeroPlaca = numeroPlaca;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agente other = (Agente) obj;
        if (this.numeroPlaca != other.numeroPlaca) {
            return false;
        }
        return true;
    }

}
