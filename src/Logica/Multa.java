package Logica;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Multa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idMultaM;
    private int valor;
    @Temporal(TemporalType.DATE)
    private Date date;
    private Vehiculo vehiculo1;
    private Persona persona1;
    private Agente agente1;
    private List<MotivoMulta> multas = new LinkedList<>();

    public Multa() {
    }

    public Multa(int valor, Date date, Vehiculo vehiculo1, Persona persona1, Agente agente1) throws Exception {
        this.date = date;
        setValor(valor);
        setVehiculo1(vehiculo1);
        setPersona1(persona1);
        setAgente1(agente1);
    }

    public int getValor() {
        return valor;
    }

    public Date date() {
        return date;
    }

    public Vehiculo getVehiculo1() {
        return vehiculo1;
    }

    public Persona getPersona1() {
        return persona1;
    }

    public Agente getAgente1() {
        return agente1;
    }

    public void setValor(int valor) throws Exception {
        if (valor < 0) {
            throw new Exception("EL valor debe ser mayor a 0");
        }
        this.valor = valor;
    }

    public void setVehiculo1(Vehiculo vehiculo1) {
        this.vehiculo1 = vehiculo1;
    }

    public void setPersona1(Persona persona1) {
        this.persona1 = persona1;
    }

    public void setAgente1(Agente agente1) {
        this.agente1 = agente1;
    }

    public void add(MotivoMulta momulta) throws Exception {
        if (multas.contains(momulta)) {
            throw new Exception("La multa ya esta asignada");
        }

        this.multas.add(momulta);
    }

    public List<MotivoMulta> getMultas() {
        return multas;
    }

    public void remove(MotivoMulta momulta) {
        this.multas.remove(momulta);
    }

    public void setMultas(List<MotivoMulta> multas) {
        this.multas = multas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Multa other = (Multa) obj;
        if (this.valor != other.valor) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.vehiculo1, other.vehiculo1)) {
            return false;
        }
        if (!Objects.equals(this.persona1, other.persona1)) {
            return false;
        }
        if (!Objects.equals(this.agente1, other.agente1)) {
            return false;
        }
        if (!Objects.equals(this.multas, other.multas)) {
            return false;
        }
        return true;
    }

    public int getIdMultaM() {
        return idMultaM;
    }

    public void setIdMultaM(int idMultaM) {
        this.idMultaM = idMultaM;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
