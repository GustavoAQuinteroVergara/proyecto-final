package Logica;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
        @NamedQuery(name = "buscar_vehiculo_por_placa ",
                query = "SELECT v FROM Vehiculo v WHERE v.placa = :numeroplaca "))
public class Vehiculo implements Serializable {

    @Id
    private String placa;
    private short modelo;
    private String color;
    private String marca;

    public Vehiculo() {
    }

    public Vehiculo(String placa, short modelo, String color, String marca) throws Exception {
        setPlaca(placa);
        setModelo(modelo);
        setColor(color);
        setMarca(marca);
    }

    public String getPlaca() {
        return placa;
    }

    public short getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }

    public void setPlaca(String placa) throws Exception {
        if (placa == null || "".equals(placa.trim())) {
            throw new Exception("La placa del vehiculo no puede quedar vacio");
        }
        this.placa = placa;
    }
    int anio = LocalDate.now().getYear() + 1;

    public void setModelo(short modelo) throws Exception {
        if (modelo < 1885 || modelo > anio) {
            throw new Exception("No se puede matricular un vehiculo antes de 1885, ni mayor a " + anio);
        }
        this.modelo = modelo;
    }

    public void setColor(String color) throws Exception {
        if (color == null || "".equals(color.trim())) {
            throw new Exception("No se puede dejar el color del vehiculo en blanco");
        }
        this.color = color;
    }

    public void setMarca(String marca) throws Exception {
        if (marca == null || "".equals(marca.trim())) {
            throw new Exception("No se puede dejar la marca del vehiculo en blanco");
        }
        this.marca = marca;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (this.modelo != other.modelo) {
            return false;
        }
        if (this.anio != other.anio) {
            return false;
        }
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        if (!Objects.equals(this.color, other.color)) {
            return false;
        }
        if (!Objects.equals(this.marca, other.marca)) {
            return false;
        }
        return true;
    }

}
