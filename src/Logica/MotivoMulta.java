package Logica;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
        @NamedQuery(name = "buscar_codigo_multa",
                query = "SELECT m FROM MotivoMulta m WHERE m.codigo =:codigo"))
public class MotivoMulta implements Serializable {

    @Id
    private String codigo;
    private String descripcion;
    private int valor;

    public MotivoMulta() {
    }

    public MotivoMulta(String codigo, String descripcion, int valor) throws Exception {
        setCodigo(codigo);
        setDescripcion(descripcion);
        setValor(valor);
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getValor() {
        return valor;
    }

    public void setCodigo(String codigo) throws Exception {
        if (codigo == null || "".equals(codigo)) {
            throw new Exception("El codigo debe ser mayor a 0");
        }
        this.codigo = codigo;
    }

    public void setDescripcion(String descripcion) throws Exception {
        if (descripcion == null || "".equals(descripcion)) {
            throw new Exception("La descripcion no puede quedar vacia");
        }
        this.descripcion = descripcion;
    }

    public void setValor(int valor) throws Exception {
        if (valor < 0) {
            throw new Exception("El valor debe ser mayor a 0");
        }
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MotivoMulta other = (MotivoMulta) obj;
        if (this.valor != other.valor) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getDescripcion();
    }

}
