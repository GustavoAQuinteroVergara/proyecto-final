package Logica;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
        @NamedQuery(name = "buscar_identificacion",
                query = "SELECT a FROM Persona a WHERE a.identificacion = :identificacion"))
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Persona implements Serializable {

    @Id
    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona() {
    }

    public Persona(long identificacion, String nombre, String apellido) throws Exception {
        setIdentificacion(identificacion);
        setNombre(nombre);
        setApellido(apellido);
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setIdentificacion(long identificacion) throws Exception {
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) throws Exception {
        if (nombre == null || "".equals(nombre.trim())) {
            throw new Exception("El nombre no puede quedar vacio");
        }
        this.nombre = nombre;
    }

    public void setApellido(String apellido) throws Exception {
        if (apellido == null || "".equals(apellido.trim())) {
            throw new Exception("El nombre no puede quedar vacio");
        }
        this.apellido = apellido;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.identificacion != other.identificacion) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        return true;
    }

}
