package Logica;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries(
        @NamedQuery(name = "buscaridentifi",
                query = "SELECT l FROM Licencia l WHERE l.persona2.identificacion = :identificacion"))
public class Licencia implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idLicencia;
    @OneToOne
    private Persona persona2;
    private Categoria cat;
    @Temporal(TemporalType.DATE)
    private Date fechaexpedicion;
    @Temporal(TemporalType.DATE)
    private Date fechavencimiento;

    public Licencia() {
    }

    public Licencia(Persona persona2, Categoria cat, Date fechaexpedicion, Date fechavencimiento) {
        this.persona2 = persona2;
        this.cat = cat;

        this.fechaexpedicion = fechaexpedicion;
        this.fechavencimiento = fechavencimiento;
    }

    public Persona getPersona2() {
        return persona2;
    }

    public Categoria getCat() {
        return cat;
    }

    public Date getFechaexpedicion() {
        return fechaexpedicion;
    }

    public void setFechaexpedicion(Date fechaexpedicion) {
        this.fechaexpedicion = fechaexpedicion;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public void setPersona2(Persona persona2) {
        this.persona2 = persona2;
    }

    public void setCat(Categoria cat) {
        this.cat = cat;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Licencia other = (Licencia) obj;
        if (!Objects.equals(this.persona2, other.persona2)) {
            return false;
        }
        if (this.cat != other.cat) {
            return false;
        }
        return true;
    }

    public int getIdLicencia() {
        return idLicencia;
    }

    public void setIdLicencia(int idLicencia) {
        this.idLicencia = idLicencia;
    }

}
